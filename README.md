# godirserver

Tiny web server that hosts a single directory (e.g. the directory in which you add your go html coverage report).

To install use `go install gitlab.com/tploss/godirserver@latest` outside of a module folder.