package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	var path string
	flag.StringVar(&path, "directory", "coverage", "directory to be served")
	var port int
	flag.IntVar(&port, "port", 9000, "port to use for hosting")
	flag.Parse()

	// check directory to be served available
	stat, err := os.Stat(path)
	if err != nil {
		log.Fatalf("Path %s could not be opened, error: %v", path, err)
	}
	if !stat.IsDir() {
		log.Fatalf("Path is not a directory: %s", path)
	}

	// check for index.html to inform
	if stat, err := os.Stat(path + "/" + "index.html"); err != nil || stat.IsDir() {
		fmt.Fprintln(os.Stderr, "Directory to serve does not contain an index.html file.")
	}

	addr := fmt.Sprintf("localhost:%d", port)
	fs := http.FileServer(http.Dir(path))

	// start HTTP server with `fs` as the default handler
	log.Fatal(http.ListenAndServe(addr, fs))

}
